#!ruby

require 'faraday'
require 'json'
require 'logger'
require 'optparse'
#require 'sinatra'

class TransifexAPI
	def initialize( user: nil, pass: nil, token: nil, targetLanguage: "fr", logLevel: Logger::INFO )
		@transcripteur = Logger.new(STDOUT)
		@transcripteur.level = logLevel

		@baseUrl = 'https://www.transifex.com'
		@language = targetLanguage
		@tableauRessources = {}

		if ( token && token != "" )
			@credentials = { :user => "api", :pass => token }
		elsif ( user && pass )
			@credentials = { :user => user, :pass => pass }
		else
			@transcripteur.info "ERREUR: Si vous n'utilisez pas de jeton de connexion, vous devez indiquer un identifiant ET un mot de passe"
		end

		@serveur = Faraday.new(:url => @baseUrl) do |c|
			c.use Faraday::Request::BasicAuthentication, @credentials[:user], @credentials[:pass]
			c.request :multipart
			c.use Faraday::Request::UrlEncoded
			# En mode debogage, on affiche les échanges réseau avec le site de Transifex
			if logLevel == Logger::DEBUG
				c.use Faraday::Response::Logger, @transcripteur, bodies:true
			end
			c.use Faraday::Response::RaiseError
			c.use Faraday::Adapter::NetHttp
		end
	end


	def listeRessources( nomProjet )
		response = @serveur.get "/api/2/project/#{nomProjet}/resources"
		toutesRessources = JSON.parse(response.body)
		toutesRessources.each do |rsrc|
			if rsrc["source_language_code"] != "en"
				nil
			elsif @tableauRessources[rsrc["slug"]]
				if ( @tableauRessources[rsrc["slug"]][:nom] == rsrc["name"] && @tableauRessources[rsrc["slug"]][:typeRsc] == rsrc["i18n_type"] )
					@tableauRessources[rsrc["slug"]][:projets].push(nomProjet)
#					@transcripteur.debug "AJOUT DE projet #{nomProjet} à la ressource #{rsrc["slug"]}"
				else
					@transcripteur.info "ERREUR: La ressource #{rsrc["name"]} a des caractéristiques différentes entre les projets à comparer"
					@transcripteur.debug "Enregistré: #{@tableauRessources[rsrc["slug"]]}"
					@transcripteur.debug "Nouveau   : #{rsrc}"
					@transcripteur.debug "-----------------------------------------------"
				end
			else
				@tableauRessources[rsrc["slug"]] = { :nom => rsrc["name"], :typeRsc => rsrc["i18n_type"], :projets => [] }
					@tableauRessources[rsrc["slug"]][:projets].push(nomProjet)
			end
		end
#		@transcripteur.debug "Ressources as of #{nomProjet} are #{@tableauRessources}"
	end


	def listeRessourcesUniques()
		tableauTemp = []
		@tableauRessources.each do |key, value|
			if value[:projets].count = 1
				elementTemp = { :clef => key, :projet => value[:projets], :nom => value[:nom]}
				tableauTemp.push elementTemp
			end
		end
		return tableauTemp
	end


	def listeRessourcesCommunes()
		tableauTemp = []
		@tableauRessources.each do |key, value|
			if value[:projets].count > 1
				elementTemp = { :clef => key, :projet => value[:projets], :nom => value[:nom]}
				tableauTemp.push elementTemp
			end
		end
		return tableauTemp
	end


	def extractionRessources( nomProjet, resourceName )
		response = @serveur.get "/api/2/project/#{nomProjet}/resource/#{resourceName}/translation/#{@language}"
		transifexContents = JSON.parse(response.body)
		@transcripteur.info "Retrieved #{resourceName} strings for #{nomProjet}"

		output = File.new("#{nomProjet}-#{resourceName}.txt", "w+")
		output.puts transifexContents["content"]
		output.close()
	end
end

class ComparateurPO
	def initialize( logLevel: Logger::INFO )
		@transcripteur = Logger.new(STDOUT)
		@transcripteur.level = logLevel

		@messages = {}
		@compteur = {:ellipsis => 0, :orphelins => 0, :differences => 0, :egalite => 0}
	end

	def chargement( cheminRessource, resourceName, nomProjet )
		msgEntry = nil
		currentMsgId = nil
		currentMsgStr = ""
		currentStep = "None"
		File.open( cheminRessource ) do |f|
			f.each_line do |line|
				if line.match(/^#: /)
					currentStep = "newMsg"
					msgEntry = { "location" => line[3..-1].strip, "ressource" => resourceName }
					currentMsgId = nil
					currentMsgStr = ""
#					@transcripteur.info "Found a new string - #{msgEntry}"
# msgid_plural "{parameterList} and {linkStart}%n more{linkEnd}"
#msgstr[0] "{parameterList} et {linkStart}%n autre{linkEnd}"
#msgstr[1] "{parameterList} et {linkStart}%n autres{linkEnd}"
				elsif line.match(/^msgid_plural /)
					if currentStep == "withinMsgId"
						@transcripteur.info "Found plural input #{line} at step #{currentStep}"
						msgEntry["avecpluriel"] = line[14..-3]
#						currentStep = "withinMsgId"
#						currentMsgId = line[7..-3]
#						@transcripteur.debug "Parsing ID from **#{line}**"
#						@transcripteur.debug "currentMsgId now is **#{currentMsgId}**"
					end
				elsif line.match(/^msgstr\[ /)
					@transcripteur.info "Found plural output #{line}"
				elsif line.match(/^msgid /)
					if currentStep != "None"
						currentStep = "withinMsgId"
						currentMsgId = line[7..-3]
						@transcripteur.debug "Parsing ID from **#{line}**"
						@transcripteur.debug "currentMsgId now is **#{currentMsgId}**"
					end
				elsif line.match(/^msgstr /)
					if currentStep != "None"
						currentStep = "withinMsgStr"
						@transcripteur.debug "Parsing Msg from **#{line}**"
						currentMsgStr = line[8..-3]
						@transcripteur.debug "currentMsgStr now is **#{currentMsgStr}**"
					end
				elsif line.match(/^$/)
					if (currentStep && currentStep == "withinMsgStr" && currentMsgId && msgEntry) # On vient de terminer de traiter une ligne
						@transcripteur.debug "REGISTERING ID #{currentMsgId}"
						if (@messages[currentMsgId])
							@transcripteur.debug "#{currentMsgId} already exists"
							@messages[currentMsgId][nomProjet] = currentMsgStr
							currentMsgId = nil
							msgEntry = nil
						else
							@messages[currentMsgId] = msgEntry
							msgEntry[nomProjet] = currentMsgStr
							currentMsgId = nil
							msgEntry = nil
						end
					else
						@transcripteur.info 'Empty line, ignoring'
					end
				else
					case currentStep
						when "None" then begin
							@transcripteur.debug "Skipping"
						end
						when "withinMsgStr" then begin
							currentMsgStr = "#{currentMsgStr}#{line[1..-3]}"
							@transcripteur.debug "currentMsgStr now is **#{currentMsgStr}**"
						end
						when "withinMsgId" then begin
							currentMsgId = "#{currentMsgId}#{line[1..-3]}"
							@transcripteur.debug "currentMsgId now is **#{currentMsgId}**"
						end
						else nil #@transcripteur.info "Suspicious contents, line: **#{line}**"
					end
				end
			end
		end
		if (currentMsgId && msgEntry) # Il en restait un !
			@transcripteur.debug "REGISTERING ID #{currentMsgId}"
			if (@messages[currentMsgId])
				@transcripteur.debug "#{currentMsgId} already exists"
				@messages[currentMsgId][nomProjet] = currentMsgStr
				currentMsgId = nil
				msgEntry = nil
			else
				@messages[currentMsgId] = msgEntry
				msgEntry[nomProjet] = currentMsgStr
				currentMsgId = nil
				msgEntry = nil
			end
		end
	end # Chargement

	def comparaison
		@compteur[:chaines] = @messages.count
		@messages.each do |key, value|
			if value["owncloud"] == value["nextcloud"]
				@messages[key]["egalite"] = true
				@compteur[:egalite] += 1
#				@transcripteur.debug "#{key} has the same translation in both projects"
			elsif (
					value["owncloud"]  && value["owncloud"]  != "" && value["owncloud"]  != "\"" &&
					value["nextcloud"] && value["nextcloud"] != "" && value["nextcloud"] != "\""
				)
				@messages[key]["egalite"] = false
				@compteur[:differences] += 1
#				@transcripteur.info "#{key} is translated DIFFERENTLY"
	#			@transcripteur.info value
			else
				@messages[key]["egalite"] = false
				@compteur[:orphelins] += 1
#				@transcripteur.info "#{key} is translated in only one project"
			end
			if /\.\.\./.match(value["owncloud"]) || /\.\.\./.match(value["nextcloud"])
				@messages[key]["ellipsis"] = true
				@compteur[:ellipsis] += 1
			end
		end
	end

	def resume
		@transcripteur.info "Il y a #{@compteur[:ellipsis]} chaines qui contiennent trois points au lieu du caractère ellipsis"
		puts @compteur
	end

	def dump
		sortie = File.open("resultat.json", "w+")
		sortie.puts @messages
		sortie.close()
	end
end # Class ComparateurPO


STDOUT.sync = true
transcripteur = Logger.new(STDOUT)
transcripteur.level = Logger::INFO

identification = {}
args = OptionParser.new do |opts|
	opts.banner = "utilisation : #{File.basename(__FILE__)} [options]"
	opts.separator ""
	opts.separator "Options possibles:"
	opts.on('-j', '--jeton JETON', String, 'Jeton de connexion à Transifex') do |v|
		identification[:jeton] = v
	end
	opts.on('-c', '--compte IDENTIFIANT', String, 'Identifiant du compte utilisateur Transifex') do |v|
		identification[:nom] = v
	end
	opts.on('-s', '--secret MOTDEPASSE', String, 'Mot de passe pour la connexion à Transifex') do |v|
		identification[:secret] = v
	end
	opts.on("-h", "--help", "Affiche le message d'aide") do
		puts opts
		exit
	end
end
args.parse!(ARGV)

dossierDeTravail = "./DevProjects/" # TODO A lire depuis ENV[TEMP] si dispo?

if identification[:jeton]
	# TODO Désactivé temporairement car déjà fait en local, reste à affiner l'utilisation des ressources'
	#ressourcesOwnNextClouds = TransifexAPI.new( :token => identification[:jeton] )
elsif identification[:nom] && identification[:secret]
	ressourcesOwnNextClouds = TransifexAPI.new( :user => identification[:nom], :pass => identification[:secret] )
else
	transcripteur.error "Vous devez fournir un moyen d'identification à Transifex"
	puts args
	exit
end
#ressourcesOwnNextClouds.listeRessources( "nextcloud" )
#ressourcesOwnNextClouds.listeRessources( "owncloud" )

comparateurTraductions = ComparateurPO.new()

[{:clef=>"twofactor_totp", :projet=>["nextcloud", "owncloud"], :nom=>"twofactor_totp"},
{:clef=>"updatenotification", :projet=>["nextcloud", "owncloud"], :nom=>"updatenotification"},
{:clef=>"user_ldap", :projet=>["nextcloud", "owncloud"], :nom=>"user_ldap"}].each do |r|
# TODO Désactivé temporairement car déjà fait en local, reste à affiner l'utilisation des ressources'
#ressourcesOwnNextClouds.listeRessourcesCommunes().each do |r|
	r[:projet].each do |projet|
		# TODO Désactivé temporairement car déjà fait en local, reste à affiner l'utilisation des ressources'
#		ressourcesOwnNextClouds.extractionRessources( projet, r[:nom] )
#		transcripteur.info "#{projet}-#{r[:nom]}.txt"
		comparateurTraductions.chargement( "#{projet}-#{r[:nom]}.txt", r[:nom], projet )
	end
	comparateurTraductions.comparaison()
	comparateurTraductions.dump()
	comparateurTraductions.resume()
end


exit 0